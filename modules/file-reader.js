//File Reader Module
//Description: 
//Dependencies
//getFileInfo
const _fs = require('fs');
const _path = require('path');
module.exports.fileReader = new fileReader();
// funcion que permite obtener la informacion del archivo
function fileReader(){
	this.isExist=function(path){
		try{
			 return _fs.statSync(path);
			}catch(e){
				return false;
			}
		};
	 //IsFile
 	this.isFile = function(path){
		try{ 
			return _fs.statSync(path).isFile();
		}catch(e){ 
			return false;
		}
	 };
	 //Directory
 	this.isDir = function(path){
		try      { 
			return _fs.statSync(path).isDirectory();
		}catch(e){ 
			return false;
		}
	 };
	 //Parse path
 	this.parsePath= function( ...paths ){
		const pathsx = paths.map(v=>{
			if( !v ) {return'.';}
			v.indexOf('~')>-1 && (v.replace(/^~/g, process.env.HOME));
			return v;
	 	});

		const exp2 =  pathsx.length===1?pathsx[0]:_path.join(...pathsx);
		const pat  = pathsx.length===0 ? '.' : exp2;
		const res  = _path.parse( _path.resolve(pat) );
		res.path = _path.join(res.dir, res.base);
		const sta  = this.isExist(res.path);
		const expl2 = sta.isDirectory() ? 'folder' : '';
		const expl1 = sta.isFile() ? 'file' : expl2;
		res.type = sta ? expl1 : null;
		return res;
 	};
	 //createDirectory
	this.createDir= function(path){
		if ( !this.isExist(path) ) {
			this.createDir( this.parsePath(path).dir );
			_fs.mkdirSync(path);
		}
	 };
	 //FileRead 
 	this.fileRead= function(isText=true){
		try{ 
			_fs.readFileSync(path, isText?'UTF8':undefined);
		}
		catch(e){
			return null;
		}

		if( isText==='lower' ) {
			return fil.toLowerCase();}
		else if ( isText==='upper' ) {
			return fil.toUpperCase();}

		return fil;
	 };
	 //FileTravel
 	this.fileTravel= function(path, recursive){
		const sta=isExist(path);

		if( !sta ) {
			return[];}
		else if ( sta.isFile() ){ 
			return[ this.parsePath(path) ];}
		else if ( sta.isDirectory() ) {
			const res=[];
			_fs.readdirSync(this.path).forEach(v=>{
				const par=this.parsePath(path, v);
				res.push(par);
				if ( par.type==='folder' && recursive ){
					par.childs=FileTravel(par.path);}
			});
			return res;
		}
	return[];
 };
}