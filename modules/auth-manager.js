/**
 * Modulo manejador de autenticación con OAuthServer
 * La finalidad del módulo es proporcionar los métodos necesarios
 * para la solicitud y renovación del access token que servirá de llave 
 * para utilizar el gateway ligero
 */
const _fetch = require('node-fetch');
const _btoa = require('btoa');
const _utils = require('./utilities.js');

module.exports.authManager = new authManager();

/**
 * Funcion que valida el oauth
 */
function authManager(){
    this.accessToken=null;
    this.accessTokenDate=null;
    this.tokenGenTrial=1;
    this.basicToken=null;

    /**
     * Funcion que inicializa la autenticacion
     */
    this.init = async function(){
        if(_globalConfig.oauthServer.url && _globalConfig.oauthServer.clientId && _globalConfig.oauthServer.clientSecret){
                await this.getBearer();
                this.tokenGenTrial = 1;
                return true;
        }else{
            console.info("ERROR "+_utils.utilities.getActualTime()+" CONFIG_PROPERTIES_ERROR");
            this.accessToken=null;
            return false;
        }
    };

    /**
     * Funcion que actualiza el access token
     * en caso de haber expirado
     */
    this.refreshToken = async function(){
        try{
            if(this.accessToken !== null && this.tokenGenTrial <= 3){
                this.basicToken=_btoa(`${_globalConfig.oauthServer.clientId}:${_globalConfig.oauthServer.clientSecret}`);
                const res = await _fetch(
                    _globalConfig.oauthServer.url + '?client_id='+_globalConfig.oauthServer.clientId,
                        {
                            method : 'POST',
                            headers: {
                                'accept'      : 'application/json',
                                'content-type': 'application/x-www-form-urlencoded',
                                'Authorization': `Basic ${this.basicToken}`
                            }
                        }
                    );
                    console.info("INFO "+_utils.utilities.getActualTime()+" REFRESH_TOKEN "+res.status);
                    if ( res.status===200 ) {
                        const jso = await res.json();
                        var atExpDate = new Date();
                        atExpDate.setSeconds(atExpDate.getSeconds() + jso.expires_in);
                        this.accessTokenDate = atExpDate;
                        this.accessToken = `Bearer ${jso.access_token}`;        
                        return true;
                    }else{
                        this.tokenGenTrial=this.tokenGenTrial+1;
                    }
            } else {
                return false;
            }
        } catch (e){
            console.info("ERR "+_utils.utilities.getActualTime()+" TOKEN_INVALIDO",e);
            return false;
        }   
    };

     /**
     * Funcion que inicializa la autenticacion
     */
    this.getBearer = async function(){
        try {
            if( this.tokenGenTrial > 3){
                return false;
            }else if( this.accessToken === null && (this.accessTokenDate === null || this.accessTokenDate > new Date()+1)){
                this.basicToken=_btoa(`${_globalConfig.oauthServer.clientId}:${_globalConfig.oauthServer.clientSecret}`);
                const res = await _fetch(
                    _globalConfig.oauthServer.url + '?client_id='+_globalConfig.oauthServer.clientId,
                        {
                            method : 'POST',
                            headers: {
                                'accept'      : 'application/json',
                                'content-type': 'application/x-www-form-urlencoded',
                                'Authorization': `Basic ${this.basicToken}`
                            }
                        }
                    ); 
                    console.info("INFO "+_utils.utilities.getActualTime()+" OAUTH_SERVE "+res.status);
                    if ( res.status===200 ) {
                        const jso = await res.json();
                        var atExpDate = new Date();
                        atExpDate.setSeconds(atExpDate.getSeconds() + jso.expires_in);
                        this.accessTokenDate = atExpDate;
                        this.accessToken = `Bearer ${jso.access_token}`;        
                        return true;
                    }else{
                        this.tokenGenTrial=this.tokenGenTrial+1;
                    }
                }else{
                    return true;
                }
            } catch(e){
                console.info("ERROR "+_utils.utilities.getActualTime()+" OBTENER_ACCESS_TOKEN ",e);
                return false;
            }
     };
}
