'use strict';
global._redis = require('redis');
global._redisStore = require('connect-redis');
global._redisC = null;
global._store =  null;

module.exports.redisClient = new redisClient();

function redisClient() {
    this.createStore = async function() {
        let _store;
        if(_properties.redis.port !== null && _properties.redis.port !== 0 && _properties.redis.host !== null && _properties.redis.host !== ''){
            // A console.log("Redis port "+_properties.redis.port)
            // B console.log("Redis host "+_properties.redis.host)
            
            const _redisC   = _redis.createClient(_properties.redis.port, _properties.redis.host);
            // C console.log("Redis _redisC "+_redisC)
            // D _redisC.on('error', err => console.info('Redis error: ', err)) 
            // A _redisC ? (_store = new _redisStore({ host:_properties.redis.host, port:_properties.redis.port, client:_redisC, ttl: 86400 }))
            // A : _store = undefined;
            if(_redisC){
                _store = new _redisStore({ host:_properties.redis.host, port:_properties.redis.port, client:_redisC, ttl: 86400 });
            }
        }
        return _store;
    };
    
}
