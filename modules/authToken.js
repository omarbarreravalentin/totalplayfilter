/**
 * Modulo manejador de autenticación con oauthToken
 * La finalidad del módulo es proporcionar
 * los métodos necesarios
 * para la solicitud y renovación
 * del access token que servirá de llave para utilizar
 * el token Opaco
 */

const _fetch = require('node-fetch');
const https = require('https');
const http = require('http');
const _utils = require('./utilities.js');
const {URLSearchParams}=require('url');
module.exports.authToken = new authToken();

/**
 * Funcion que valida el oauth del token opaco
 */
function authToken() {
    this.accessToken = null;
    this.accessTokenDate = null;
    this.tokenGenTrial = 1;

    /**
     * Funcion que inicializa la autenticacion del token opaco
     */
    this.init = async function () {
        if (_globalConfig.oauthToken.url && _globalConfig.oauthToken.clientId && _globalConfig.oauthToken.clientSecret) {
            this.tokenGenTrial = 1;
            return await this.getBearer();
        } else {
            console.info("ERROR "+_utils.utilities.getActualTime()+" OAUTH_CREDENTIALS_UNDEFINED");
            this.accessToken = null;
            return false;
        }
    };

    /**
     * Funcion que actualiza el access token del token opaco
     * en caso de haber expirado
     */
    this.refreshToken = async function () {
        if (this.accessToken !== null && this.tokenGenTrial <= 3) {
            try {
                const form = new URLSearchParams();
                form.append('scope', "oauth-token-management_1.0.0");
                form.append('grant_type', _globalConfig.oauthToken.grantType);
                form.append('client_id', _globalConfig.oauthToken.clientId);
                form.append('client_secret', _globalConfig.oauthToken.clientSecret);
                let agent = new https.Agent({
                    rejectUnauthorized: false
                });
                const entorno=process.env.NODE_ENV || 'local';
                if(entorno=='local'){
                    agent = new http.Agent({
                        rejectUnauthorized: false
                    });
                }
                const res = await _fetch(_globalConfig.oauthToken.url,
                    {
                        agent,
                        method: 'POST',
                        body: form
                    }
                );
                console.info("INFO "+_utils.utilities.getActualTime()+" OAUTH_REFRESH "+res.status);
                if (res.status === 200) {
                    const jso = await res.json();
                    var atExpDate = new Date();
                    atExpDate.setSeconds(atExpDate.getSeconds() + jso.expires_in);
                    this.accessTokenDate = atExpDate;
                    this.accessToken = `Bearer ${jso.access_token}`;
                    return true;
                } else {
                    this.tokenGenTrial = this.tokenGenTrial + 1;
                    return false;
                }
            } catch (error) {
                console.info("ERROR "+_utils.utilities.getActualTime()+" OAUTH_REFRESH "+error);
                return false;
            }
        } else {
            return false;
        }
    };

     /**
     * Funcion que inicializa la autenticacion
     */
    this.getBearer = async function () {
        if (this.tokenGenTrial > 3) {
            return false;
        } else if (this.accessToken === null && (this.accessTokenDate === null || this.accessTokenDate > new Date() + 1)) {
            try {
                const form = new URLSearchParams();
                form.append('scope', "oauth-token-management_1.0.0");
                form.append('grant_type', _globalConfig.oauthToken.grantType);
                form.append('client_id', _globalConfig.oauthToken.clientId);
                form.append('client_secret', _globalConfig.oauthToken.clientSecret);
                let agent = new https.Agent({
                    rejectUnauthorized: false
                });
                const entorno=process.env.NODE_ENV || 'local';
                if(entorno=='local'){
                    agent = new http.Agent({
                        rejectUnauthorized: false
                    });
                }
                const res = await _fetch(_globalConfig.oauthToken.url,
                    {
                        agent,
                        method: 'POST',
                        body: form
                    }
                );
                console.info("INFO "+_utils.utilities.getActualTime()+" OAUTH_TOKEN "+res.status);
                if (res.status === 200) {
                    const jso = await res.json();
                    var atExpDate = new Date();
                    atExpDate.setSeconds(atExpDate.getSeconds() + jso.expires_in);
                    this.accessTokenDate = atExpDate;
                    this.accessToken = `Bearer ${jso.access_token}`;
                    return true;
                } else {
                    this.tokenGenTrial = this.tokenGenTrial + 1;
                    return false;
                }
            } catch (error) {
                console.info("ERROR "+_utils.utilities.getActualTime()+" OAUTH_TOKEN "+error);
                return false;
            }
        } else {
            return true;
        }
    };
}
