/**
 * ENDPOINTS DE COMUNICACION CON LOS MICROSERVICIOS
 * FUNCIONA COMO UN MIDDLEWARE
 */
const _https = require('https');
const _http = require('http');
const _fetch = require('node-fetch');
const FormData = require('form-data');
const querystring = require('querystring');
const _authManager = require("./auth-manager");
const _authToken = require("./authToken");
const validUrl = require('valid-url');
const _utils = require('./utilities.js');
module.exports.endPoints = new endPoints();
module.exports.endPointsChunks = new endPointsChunks();

/**
 * Funcion que realiza las conexiones
 * con los microservicios
 */
function endPoints() {

    /**
     * funcion que obtiene el carchivo de configuracion
     * desde un config-cloud
     */
    this.getConfig=async function(){
        const ruta=process.env.NODE_CLOUD_CONFIG_URI+"/"+process.env.NODE_CLOUD_CONFIG_NAME+"-"+process.env.NODE_CLOUD_CONFIG_PROFILE+".json";
        const res=_fetch(ruta, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
        return res;
    }

    this.getOCR = async function (idTransaccion) {
        const ruta = "https://api-work.eu.veri-das.com/validas/v1/validation/" + idTransaccion + "/ocr";
        const res = _fetch(ruta, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'apikey': "n6M7Ug5pYPEWVCaUSZZHncZsCJhcXKafqFuK"
            }
        });
        return res;
    }

    this.getSCORES = async function (idTransaccion) {
        const ruta = "https://api-work.eu.veri-das.com/validas/v1/validation/" + idTransaccion + "/scores";
        const res = _fetch(ruta, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'apikey': "n6M7Ug5pYPEWVCaUSZZHncZsCJhcXKafqFuK"
            }
        });
        return res;
    }


    /**
     * SE COMUNICA CON EL DOSSIER PERMITIENDO CREAR EL EXPEDIENTE
     * CUANDO EL TOKEN ES VALIDO
     */
    this.validaAccessToken = async function (tokenO) {
        const tokenOpaco=tokenO.replace(/ /g, "+");
        let ready = await _authToken.authToken.init();
        let currentTime = new Date();
        if (currentTime > _authToken.authToken.accessTokenDate) {
            ready = await _authToken.authToken.refreshToken();
        }
        if (ready) {
            try {
                let agent = new _https.Agent({
                    rejectUnauthorized: false
                });
                const entorno=process.env.NODE_ENV || 'local';
                if(entorno=='local'){
                    agent = new _http.Agent({
                        rejectUnauthorized: false
                    });
                }
                const res = await _fetch(
                    _globalConfig.tokenOpacoValidator.url, {
                    agent,
                    method: 'POST',
                    body:JSON.stringify({
                        "SecObjSel": {
                            "SecObjValue":tokenOpaco
                        }}),
                    headers: {
                        'accept': 'application/json',
                        'Content-Type': 'application/json',
                        'x-ibm-client-id': _globalConfig.oauthToken.clientId,
                        'MsgRqHdr': JSON.stringify({"ClientIPAddress": "127.0.0.1"}),
                        'Authorization': `${_authToken.authToken.accessToken}`
                    }
                });
                console.info("INFO "+_utils.utilities.getActualTime()+" TOKENOPACO_VALIDATOR "+res.status);
                if (res.status === 200) {
                    const jso = await res.json();
                    let resultado={
                        idExpediente:undefined,
                        idCanal:undefined,
                        idOrigen:undefined,
                        idPais:undefined,
                        redirectURL:undefined
                    };
                    if(jso.hasOwnProperty("SecObjRec")){
                        if(jso.SecObjRec.hasOwnProperty("SecObjInfoBean")){
                            let SecObjData=jso.SecObjRec.SecObjInfoBean;
                            if(SecObjData.hasOwnProperty("SecObjData")){
                                let info=SecObjData.SecObjData;
                                info.forEach(element => {
                                    if(element.SecObjDataKey==="redirectURL"){
                                        if (validUrl.isUri(element.SecObjDataValue)){
                                            resultado[element.SecObjDataKey]=element.SecObjDataValue;
                                        }
                                        else {
                                            throw Error("BAD_URL");
                                        }
                                    }else if(element.SecObjDataKey==="idExpediente" ||
                                    element.SecObjDataKey==="idCanal" ||
                                    element.SecObjDataKey==="idOrigen" ||
                                    element.SecObjDataKey==="idPais"){
                                        resultado[element.SecObjDataKey]=element.SecObjDataValue;
                                    }
                                });
                                Object.keys(resultado).forEach(element=>{
                                    if (typeof resultado[element] === "undefined") {
                                        console.info("ERROR "+_utils.utilities.getActualTime()+" UNDEFINED "+element);
                                        throw Error(element+"_UNDEFINED");
                                    }
                                });
                                return resultado;
                            }
                        }
                    }
                }else{
                    const ab=await res.json();
                    let causa=res.status;
                    if(typeof ab=="object"){
                        if(ab.hasOwnProperty("Status")){
                            causa=ab.Status.StatusDesc;
                        }
                    }
                    console.info("ERROR "+_utils.utilities.getActualTime()+" "+ab);
                    console.info("ERROR "+_utils.utilities.getActualTime()+" "+causa);
                    throw Error(causa);
                }
            } catch (error) {
                throw Error(String(error));
            }
        } else {
            console.info("ERROR "+_utils.utilities.getActualTime()+" ACCESS_TOKEN_BAD_CREDENTIALS");
            throw Error("ERR_CREDENTIALS");
        }
        console.info("ERROR "+_utils.utilities.getActualTime()+" PARAMETRO_INCOMPLETO");
        throw Error("PARAMETRO_INCOMPLETO");
    };

    this.createExpediente = async function (obj) {
        /*let ready = await _authManager.authManager.init();
        let currentTime = new Date();
        if (currentTime > _authManager.authManager.accessTokenDate) {
            ready = await _authManager.authManager.refreshToken();
        }
        if (ready) {*/
            let response = await this.prepareExpediente(obj);
            return response;
        /*}
        //TODO RETURN SOME ERROR
        console.info("ERROR "+_utils.utilities.getActualTime()+" TOKEN_NO_GENERADO");
        let err = new Error("No se pudo obtener el token");
        return err;*/
    };

    this.getExpediente = async function (idExpediente) {
        let ready = await _authManager.authManager.init();
        let currentTime = new Date();
        if (currentTime > _authManager.authManager.accessTokenDate) {
            ready = await _authManager.authManager.refreshToken();
        }
        if (ready) {
            const url=_globalConfig.backServices.dossier.direction + _globalConfig.backServices.dossier.getExpedient+"/?idExpediente="+idExpediente
            const res = await _fetch(url,
                {
                    method: 'GET',
                    headers: {
                        'Authorization': _authManager.authManager.accessToken,
                        'accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                });
            return res;
        }
        let err = new Error("No se pudo obtener el token");
        return err;
    };

    this.prepareExpediente = async function (obj) {
        /*if (_authManager.authManager.accessToken == null) {
            throw Error("Token no valido");
        } else {*/
            const creteExp=_globalConfig.backServices.dossier.direction + _globalConfig.backServices.dossier.createExpedient;
            console.log("To "+creteExp);
            const res = _fetch(creteExp,
                {
                    method: 'POST',
                    body: JSON.stringify(obj),
                    headers: {
                        //'Authorization': _authManager.authManager.accessToken,
                        'accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                });
            return res;
        //}
    };
    /**
     * ENDPOINT QUE PERMITE CARGAR EL VIDEO
     * ENVIANDO LOS DATOS ALMACENADOS EN UNA COOKIE
     */
    this.uploadVideo = async function (idExpediente, idsesion, file) {
        let currentTime = new Date();
        if (currentTime > _authManager.authManager.accessTokenDate) {
            let ready = await _authManager.authManager.refreshToken();
            if (ready) {
                const buf = Buffer.from(file, 'binary');
                const form = new FormData();
                form.append('file', buf, {
                    contentType: 'video/webm',
                    name: idExpediente + '.webm',
                    filename: idExpediente + '.webm'
                });
                form.append('idExpediente', idExpediente);
                form.append('idSesion', idsesion);
                const nheaders = Object.assign({
                    'Authorization': _authManager.authManager.accessToken
                }, form.getHeaders());
                const res = await _fetch(_globalConfig.backServices.biometricvideo.direction + _globalConfig.backServices.biometricvideo.video,
                    {
                        method: 'POST',
                        body: form,
                        headers: nheaders
                    });
                return res;

            }
        } else if (_authManager.authManager.accessTokenDate !== null) {
            const buf = Buffer.from(file, 'binary');
            const form = new FormData();
            form.append('file', buf, {
                contentType: 'video/webm',
                name: idExpediente + '.webm',
                filename: idExpediente + '.webm'
            });
            form.append('idExpediente', idExpediente);
            form.append('idSesion', idsesion);
            const nheaders = Object.assign({
                'Authorization': _authManager.authManager.accessToken
            }, form.getHeaders());
            const res = await _fetch(_globalConfig.backServices.biometricvideo.direction + _globalConfig.backServices.biometricvideo.video,
                {
                    method: 'POST',
                    body: form,
                    headers: nheaders
                });
            return res;
        }
        //TODO RETURN SOME ERROR
        let err = new Error("No se pudo obtener el token");
        return err;
    };
    /**
     * ENDPOINT QUE PERMITE CARGAR EL DOCUMENTO
     * ENVIANDO LOS DATOS ALMACENADOS EN UNA COOKIE
     */
    this.uploadDocument = async function (expediente, idSesion, file, type) {
        let currentTime = new Date();
        if (currentTime > _authManager.authManager.accessTokenDate) {
            let ready = await _authManager.authManager.refreshToken();
            if (ready) {
                const data = file.replace(/^data:image\/\w+;base64,/, "");
                const buf = Buffer.from(data, 'base64');
                const form = new FormData();
                form.append('file', buf, {
                    contentType: 'image/png',
                    name: expediente + '.png',
                    filename: expediente + '.png'
                });
                form.append('idExpediente', expediente);
                form.append('idSesion', idSesion);
                const nheaders = Object.assign({
                    'Authorization': _authManager.authManager.accessToken
                }, form.getHeaders());
                let url = _globalConfig.backServices.document.direction;
                switch (type) {
                    case "VDDocument_reverseDetection":
                        url += _globalConfig.backServices.document.documentRev;
                        break;
                    case "VDDocument_obverseDetection":
                        url += _globalConfig.backServices.document.documentAnv;
                        break;
                    default:
                        break;
                }
            }
            const res = await _fetch(url,
                {
                    method: 'POST',
                    body: form,
                    headers: nheaders
                });
            return res;
    } else if (_authManager.authManager.accessTokenDate !== null) {
        const data = file.replace(/^data:image\/\w+;base64,/, "");
        const buf = Buffer.from(data, 'base64');
        const form = new FormData();
        form.append('file', buf, {
            contentType: 'image/png',
            name: expediente + '.png',
            filename: expediente + '.png'
        });
        form.append('idExpediente', expediente);
        form.append('idSesion', idSesion);
        const nheaders = Object.assign({
            'Authorization': _authManager.authManager.accessToken
        }, form.getHeaders());
        let url = _globalConfig.backServices.document.direction;
        if (type === "VDDocument_reverseDetection") {
            url += _globalConfig.backServices.document.documentRev;
        } else if (type === "VDDocument_obverseDetection") {
            url += _globalConfig.backServices.document.documentAnv;
        }
        const res = await _fetch(url,
            {
                method: 'POST',
                body: form,
                headers: nheaders
            });
        return res;
    }
    //TODO RETURN SOME ERROR
    let err = new Error("No se pudo obtener el token");
    return err;
};
/**
 * ENDPOINT QUE PERMITE CARGAR EL SELFIE
 * ENVIANDO LOS DATOS ALMACENADOS EN UNA COOKIE
 */
this.uploadSelfie = async function (expediente, idSesion, file) {
    let currentTime = new Date();
    if (currentTime > _authManager.authManager.accessTokenDate) {
        let ready = await _authManager.authManager.refreshToken();
        if (ready) {
            const data = file.replace(/^data:image\/\w+;base64,/, "");
            const buf = Buffer.from(data, 'base64');
            const form = new FormData();
            form.append('file', buf, {
                contentType: 'image/png',
                name: expediente + '.png',
                filename: expediente + '.png'
            });
            form.append('idExpediente', expediente);
            form.append('idSesion', idSesion);
            const nheaders = Object.assign({
                'Authorization': _authManager.authManager.accessToken
            }, form.getHeaders());
            const res = await _fetch(_globalConfig.backServices.biometricselfie.direction + _globalConfig.backServices.biometricselfie.selfie,
                {
                    method: 'POST',
                    body: form,
                    headers: nheaders
                });
            return res;

        }
    } else if (_authManager.authManager.accessTokenDate !== null) {
        const data = file.replace(/^data:image\/\w+;base64,/, "");
        const buf = Buffer.from(data, 'base64');
        const form = new FormData();
        form.append('file', buf, {
            contentType: 'image/png',
            name: expediente + '.png',
            filename: expediente + '.png'
        });
        form.append('idExpediente', expediente);
        form.append('idSesion', idSesion);
        const nheaders = Object.assign({
            'Authorization': _authManager.authManager.accessToken
        }, form.getHeaders());
        const res = await _fetch(_globalConfig.backServices.biometricselfie.direction + _globalConfig.backServices.biometricselfie.selfie,
            {
                method: 'POST',
                body: form,
                headers: nheaders
            });
        return res;
    }
    //TODO RETURN SOME ERROR
    let err = new Error("No se pudo obtener el token");
    return err;
};
}
function endPointsChunks() {
    this.getAddFile = async function (req) {
        let currentTime = new Date();
        if (currentTime > _authManager.authManager.accessTokenDate) {
            let ready = await _authManager.authManager.refreshToken()
            if (ready) {
                const data = querystring.stringify(req);
                let url = _globalConfig.backServices.chunks.direction + _globalConfig.backServices.chunks.add + "?";
                url += data;
                const res = await _fetch(url, {
                    method: 'GET',
                    headers: {
                        'Authorization': _authManager.authManager.accessToken
                    }
                });
                return res;
            }
        } else if (_authManager.authManager.accessToken !== null) {
            const data = querystring.stringify(req);
            let url = _globalConfig.backServices.chunks.direction + _globalConfig.backServices.chunks.add + "?";
            url += data;
            const res = await _fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': _authManager.authManager.accessToken
                }
            });
            return res;
        }
        //TODO RETURN ERROR
    };
    this.postAddFile = async function (req) {
        let currentTime = new Date();
        if (currentTime > _authManager.authManager.accessTokenDate) {
            let ready = await _authManager.authManager.refreshToken();
            if (ready) {
                const nheaders = Object.assign({
                    'Authorization': _authManager.authManager.accessToken
                }, req.getHeaders());
                const url = _globalConfig.backServices.chunks.direction + _globalConfig.backServices.chunks.add;
                const res = await _fetch(url, {
                    method: 'POST',
                    body: req,
                    headers: nheaders
                });
                return res;
            }
        } else if (_authManager.authManager.accessToken !== null) {
            const url = _globalConfig.backServices.chunks.direction + _globalConfig.backServices.chunks.add;
            const nheaders = Object.assign({
                'Authorization': _authManager.authManager.accessToken
            }, req.getHeaders());
            const res = await _fetch(url, {
                method: 'POST',
                body: req,
                headers: nheaders
            });
            return res;
        }
        //TODO RETURN SOME ERROR
        let err = new Error("No se pudo obtener el token");
        return err;
    };

    this.postOneDocument = async function (req,type) {
      let url = _globalConfig.backServices.document.direction + _globalConfig.backServices.document.anverso;
      if(type=="VDDocument_reverseDetection"){
        url = _globalConfig.backServices.document.direction + _globalConfig.backServices.document.reverso;
      }else if(type=="VDSelfie_faceDetection"){
        url = _globalConfig.backServices.selfie.direction + _globalConfig.backServices.selfie.serious;
      }else if(type=="VDVideo_standardVideoOutput"){
        url = _globalConfig.backServices.video.direction + _globalConfig.backServices.video.android;
      }else if(type=="VDVideo_alternativeVideoOutput"){
        url = _globalConfig.backServices.video.direction + _globalConfig.backServices.video.ios;
      }
      let res = await _fetch(url, {
          method: 'POST',
          body: req,
          headers: req.getHeaders()
      });
      return res;
    };

    this.getIDValidas = async function (idExpediente,idSesion) {
      let url = _globalConfig.backServices.dossier.direction + _globalConfig.backServices.dossier.getIDValidas+"?idExpediente="+idExpediente+"&idSesion="+idSesion;
      let res = await _fetch(url, {
          method: 'GET',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
          }
      });
      return res;
    };

    this.concatFile = async function (req) {
        let currentTime = new Date();
        if (currentTime > _authManager.authManager.accessTokenDate) {
            let ready = await _authManager.authManager.refreshToken();
            if (ready) {
                const data = querystring.stringify(req);
                let url = _globalConfig.backServices.chunks.direction + _globalConfig.backServices.chunks.merge + "?";
                url += data;
                const nheaders = {
                    'Authorization': _authManager.authManager.accessToken
                }
                const res = await _fetch(url, {
                    method: 'POST',
                    headers: nheaders
                });
                return res;
            }
        } else if (_authManager.authManager.accessToken) {
            const data = querystring.stringify(req);
            let url = _globalConfig.backServices.chunks.direction + _globalConfig.backServices.chunks.merge + "?";
            url += data;
            const nheaders = {
                'Authorization': _authManager.authManager.accessToken
            }
            const res = await _fetch(url, {
                method: 'POST',
                headers: nheaders
            });
            return res;
        }
        //TODO RETURN SOME ERROR
        let err = new Error("No se pudo obtener el token");
        return err;
    };
}
