module.exports.utilities = new utilities();
function getcmd(texto){
	if(texto!==null){
		return texto
		.replace(/%r/g,  '\x1b[0m') // Reset

		.replace(/%cb/g, '\x1b[1m') // CBright
		.replace(/%cd/g, '\x1b[2m') // CDim
		.replace(/%cu/g, '\x1b[4m') // CUnderscore
		.replace(/%cl/g, '\x1b[5m') // CBlink
		.replace(/%cv/g, '\x1b[7m') // CReverse
		.replace(/%ch/g, '\x1b[8m') // CHidden

		.replace(/%fb/g, '\x1b[30m') // FgBlack
		.replace(/%fr/g, '\x1b[31m') // FgRed
		.replace(/%fg/g, '\x1b[32m') // FgGreen
		.replace(/%fy/g, '\x1b[33m') // FgYellow
		.replace(/%fu/g, '\x1b[34m') // FgBlue
		.replace(/%fm/g, '\x1b[35m') // FgMagenta
		.replace(/%fc/g, '\x1b[36m') // FgCyan
		.replace(/%fw/g, '\x1b[37m') // FgWhite

		.replace(/%bb/g, '\x1b[40m') // BgBlack
		.replace(/%br/g, '\x1b[41m') // BgRed
		.replace(/%bg/g, '\x1b[42m') // BgGreen
		.replace(/%by/g, '\x1b[43m') // BgYellow
		.replace(/%bu/g, '\x1b[44m') // BgBlue
		.replace(/%bm/g, '\x1b[45m') // BgMagenta
		.replace(/%bc/g, '\x1b[46m') // BgCyan
		.replace(/%bw/g, '\x1b[47m') // BgWhite
		;
	}else{
		return null;
	}
}

function getcode(code){
	switch (code) {
		case 200: return 'Ok';
		case 201: return 'Created';
		case 202: return 'Accepted';
		case 203: return 'Non-Authoritative Information';
		case 204: return 'No Content';
		case 205: return 'Reset Content';
		case 206: return 'Partial Content';
		case 207: return 'Multi-Status';
		case 208: return 'Multi-Status';
		case 226: return 'IM Used';

		case 300: return 'Multiple Choice';
		case 301: return 'Moved Permanently';
		case 302: return 'Found';
		case 303: return 'See Other';
		case 304: return 'Not Modified';
		case 305: return 'Use Proxy';
		case 306: return 'unused';
		case 307: return 'Temporary Redirect';
		case 308: return 'Permanent Redirect';

		case 400: return 'Bad Request';
		case 401: return 'Unauthorized';
		case 402: return 'Payment Required';
		case 403: return 'Forbidden';
		case 404: return 'Not Found';
		case 405: return 'Method Not Allowed';
		case 406: return 'Not Acceptable';
		case 407: return 'Proxy Authentication Required';
		case 408: return 'Request Timeout';
		case 409: return 'Conflict';
		case 410: return 'Gone';
		case 411: return 'Length Required';
		case 412: return 'Precondition Failed';
		case 413: return 'Payload Too Large';
		case 414: return 'URI Too Long';
		case 415: return 'Unsupported Media Type';
		case 416: return 'Requested Range Not Satisfiable';
		case 417: return 'Expectation Failed';
		case 418: return 'I\'m a teapot';
		case 421: return 'Misdirected Request';
		case 422: return 'Unprocessable Entity';
		case 423: return 'Locked';
		case 424: return 'Failed Dependency';
		case 426: return 'Upgrade Required';
		case 428: return 'Precondition Required';
		case 429: return 'Too Many Requests';
		case 431: return 'Request Header Fields Too Large';
		case 451: return 'Unavailable For Legal Reasons';

		case 500: return 'Internal Server Error';
		case 501: return 'Not Implemented';
		case 502: return 'Bad Gateway';
		case 503: return 'Service Unavailable';
		case 504: return 'Gateway Timeout';
		case 505: return 'HTTP Version Not Supported';
		case 506: return 'Variant Also Negotiates';
		case 507: return 'Insufficient Storage';
		case 508: return 'Loop Detected';
		case 510: return 'Not Extended';
		case 511: return 'Network Authentication Required';
		default:
			break;
	}
	return '';
}

function utilities(){

	this.tipode = function(type) {
    	return Object.prototype.toString.call(type).replace(/object|\[|\]| /g, '').toLowerCase();
	};
	this.cmd = function(texto) {
		return getcmd(texto);
	 };
	this.getActualTime=function(){
		const objFecha = new Date();
		const dia  = objFecha.getDate();
		const mes  = objFecha.getMonth();
		const anio = objFecha.getFullYear();
		const horas=objFecha.getHours();
		const minutos=objFecha.getMinutes();
		const segundos=objFecha.getSeconds();
		const mili=objFecha.getMilliseconds();
		return dia+"/"+mes+"/"+anio+" "+horas+":"+minutos+":"+segundos+":"+mili;
	}

 	this.Mime = function(type) {
		if ( tipode(type)==='string' ) {
			switch ( type.toLowerCase() ) {
				case '.js'  : case 'js'  : return 'application/javascript';
				case '.bh'  : case 'bh'  : return 'text/html';
				case '.bj'  : case 'bj'  : return 'application/javascript';
				case '.css' : case 'css' : return 'text/css';
				case '.ico' : case 'ico' : return 'image/x-icon';
				case '.jpg' : case 'jpg' : return 'image/jpeg';
				case '.png' : case 'png' : return 'image/png';
				case '.svg' : case 'svg' : return 'image/svg+xml';
				case '.txt' : case 'txt' : return 'text/plain';
				case '.woff': case 'woff': return 'font/woff';
				case '.html': case 'html': return 'text/html';
				case '.json': case 'json': return 'application/json';
				case '.pdf' : case 'pdf' : return 'application/pdf';
				default:'.';
			}
			if ( type[0]!=='.' ) {return type;}
		}
		return 'text/txt; null';
 	};
 	this.HttpCode = function(code) {
		return getcode(code);
 	};

 	this.AHeaders = function(request) {
		return {
			'accept'         : 'application/json',
			'content-type'   : 'application/json',
			'x-ibm-client-id': _clientID,
			'Authorization'  : request.session.bearer
		};
	 };
}

 