const _fetch = require('node-fetch');
const _auth_manager = require('./auth-manager');
const _utils = require('./utilities.js');
module.exports.tokenOpacoValidator = new tokenOpacoValidator();

/**
 * Funcion que valida el token opaco
 */
function tokenOpacoValidator(){

    /**
     * funcion que valida el token opaco
     */
    this.tokenOpacoInvoke = async function(tokenOpaco){
        const res = await _fetch(
            _globalConfig.tokenOpacoValidator.url +
            '?token='+ tokenOpaco,
                {
                    method : 'POST',
                    headers: {
                        'accept'      : 'application/json',
                        'content-type': 'application/json',
                        'access-token': `${_auth_manager.authManager.accessToken}`
                    }
                }
            );
            console.info("INFO "+_utils.utilities.getActualTime()+" TOKEN_OPACO_VALIDA "+res.status);
            if ( res.status===200 ) {
                const jso = await res.json();
                return {"isValid":true, "idExpediente": jso.idExpediente,
                                       "idCanal": jso.idCanal, "idPais":jso.idPais, 
                                    "idSesion":jso.idSesion};
            }else{
                return { _code:401, error:'El token no pudo ser validado' };
            }    
    };
    
    /**
     * funcion que valida el token opaco
     */
    this.tokenOpacoValidate = async function(tokenOpaco){
        const ready = await _auth_manager.authManager.getBearer();
        let dossier=null;
        if (ready 
            && _globalConfig.tokenOpacoValidator.url !== null && _globalConfig.tokenOpacoValidator.url !== ''){
            dossier=await this.tokenOpacoInvoke(tokenOpaco);
        }
        return dossier;
    };

    /**
     * funcion que valida el token opaco
     */
    this.tokenOpacoSync = function(tokenOpaco){
        var dossierTemp=null;
        this.tokenOpacoValidate(tokenOpaco)
        .then(dossier => {
            dossierTemp=dossier;
        })
        .catch(e =>{
            console.info('Error no se pudo validar el token opaco', e);
            dossierTemp={"isValid":false};
        });
        return dossierTemp;
    };
}

