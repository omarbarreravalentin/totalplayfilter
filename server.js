/**
 * No olvides creear la variable de entorno 
 * NODE_ENV local
 * en heroku
 */
var express = require('express');
const _https = require('https');

var port = process.env.PORT || 3000;
var app = express();
const _session = require('express-session');
const multer = require('multer');
var storage = multer.memoryStorage();
var upload = multer({
  storage: storage
});
const randomstring = require("randomstring");
const bodyParser = require('body-parser');
const _endPoints = require('./modules/endPoints.js');
const _csrf = require('csurf');
const FormData = require('form-data');
var cors = require('cors');
const fs = require('fs');
const util = require('util');
const _cookieParser = require('cookie-parser');
const _uuidv4 = require('uuid/v4');
const _config = require('config');
//const redis = require('redis');
const _helmet = require('helmet');


global._properties = _config.get(process.env.NODE_ENV || 'local');
global._globalConfig = {};


/*const redisClient = redis.createClient({
    host: "redis-18793.c100.us-east-1-4.ec2.cloud.redislabs.com",
    port: 18793,
    password:"ftXPFLPeE6faDHjfpPAVl66Tlr69u4iE"
});
const redisStore = require('connect-redis')(_session);

redisClient.on('error', (err) => {
    console.info('Redis error: ', err);
    
});*/

/** Funcion que muestra el mensaje de conexión exitosa al redis */
/*redisClient.on('ready', () => {
    console.info('Redis ready');
});*/

const parseForm = bodyParser.urlencoded({
  extended: false
})

app.use(bodyParser.json({
  limit: '25mb'
}));
app.use(cors({
  origin: [
    "https://192.168.0.21:4200",
    "https://ocr-minsait.web.app",
    "https://procesa-ocr.web.app",
    "https://procesaocr.herokuapp.com/",
    "https://ocr-totalplay.web.app",
    "http://localhost:4200/"
  ],
  credentials: true
}));
app.use(_helmet());
app.set('trust proxy', 1);
app.disable('x-powered-by');
app.use(_cookieParser("secretssssss"));
app.use((req, res, next) => {
  req.Body = () => {
    return new Promise((run) => {
      req.body = '';
      req.setEncoding('utf8');
      req.on('data', chunk => req.body += chunk);
      req.on('end', () => run(req.body));
    });
  };
  req.Json = () => {
    return new Promise((run) => {
      req.Body().then(d => {
        let rej = {};
        try {
          rej = JSON.parse(d);
        } catch (e) {}
        run(req.json = rej);
      });
    });
  };
  next();
});

app.use(
  _session({
    //store: new redisStore({ host: "redis-18793.c100.us-east-1-4.ec2.cloud.redislabs.com", port: 18793, client: redisClient, ttl: 86400}),
    name: "monster",
    secret: "secretssssss",
    resave: false,
    saveUninitialized: true,
    genid: function() {
      return _uuidv4();
    },
    proxy: true,
    cookie: {
      //domain: _properties.app.domain,
      secure: false,
      //httpOnly: true,
      maxAge: 1200000,
      signed: true
    }
  })
);

const csrfProtection = _csrf({
  cookie: true,
  maxAge: 1200000,
  path: '/',
  httpOnly: false
});


app.get('/', function(req, res) {
  res.send(JSON.stringify({
    Hello: 'omar'
  }));
});

app.get('/getToken', csrfProtection, async function(req, res) {
  res.status(200).json({
    'XSRF-TOKEN': req.csrfToken()
  });
});

app.post("/validaToken", parseForm, async function(req, res) {
  if (req.body && req.body.token) {
    const idExpediente = randomstring.generate(15);
    const idSesion = randomstring.generate(15);

    let obj = {
      idExpediente: idExpediente,
      idCanal: "ON",
      idOrigen: "1",
      idPais: "MEX",
      redirectURL: "https://www.minsait.com/es"
    };
    obj["idSesion"] = idSesion;
    obj["modoFlash"] = false;
    obj["estatus"] = "OK";
    req.session = Object.assign(req.session, obj);
    let code = 404;
    _endPoints.endPoints.createExpediente(obj)
      .then(exp => {
        code = exp.status;
        console.log("createExpedienteCode "+code);
        if (code < 205) {
          res.status(code).json({
            idExpediente: idExpediente,
            idSesion: idSesion,
            redirectURL: obj.redirectURL
          });
        } else {
          return exp.json();
        }
      }).then(dato => {
        if (dato != undefined) {
          res.status(code).send();
        }
      }).catch(er => {
        console.log("limpiando cookie ");
        console.log(er);

        if (req.session) {
          req.session.destroy();
          res.clearCookie("monster");
        }
        res.status(401).json({
          err: "createExpediente " + er
        });
      });
  } else {
    res.status(401).json({
      err: "token no recibido"
    });
  }
});

app.post("/addOneFile", upload.single('file'), parseForm, async function(req, res) {
  let idExpediente = (req.session.idExpediente) ? req.session.idExpediente : req.query["idExpediente"];
  let idSesion = (req.session.idSesion) ? req.session.idSesion : req.query["idSesion"];

  if (req.file && idExpediente && idSesion) {
    let ext = ".png";
    let type = req.query["type"];
    if (type == "VDVideo_standardVideoOutput") {
      ext = ".webm";
    } else if (type == "VDVideo_alternativeVideoOutput") {
      ext = ".json";
    }
    let theFile = req.file;
    const form = new FormData();
    form.append('file', theFile.buffer, {
      contentType: theFile.mimetype,
      name: req.session.idExpediente + ext,
      filename: req.session.idExpediente + ext
    });
    form.append('idExpediente', idExpediente);
    form.append('idSesion', idSesion);
    let code = 404;
    _endPoints.endPointsChunks.postOneDocument(form, type)
      .then(exp => {
        code = exp.status;
        console.log("code " + code);
        if (code < 205) {
          res.status(code).send();
        } else {
          try {
            return exp.text();
            //return exp.json();
          } catch (error) {
            return exp.json();
          }
        }
      }).then(dato => {
        if (dato != undefined) {
          console.log("limpiando cookies");
          if (req.session) {
            req.session.destroy();
            res.clearCookie("monster");
          }
          res.status(code).send();
        }
      }).catch(er => {
        console.log("limpiando cookies");
        if (req.session) {
          req.session.destroy();
          res.clearCookie("monster");
        }
        res.status(503).json({
          err: er
        });
      });
  } else {
    res.status(401).json({
      err: "archivoNoExiste"
    });
  }
});

app.get('/getIdValidas', parseForm, async function(req, res) {
  let idExpediente = (req.session.idExpediente) ? req.session.idExpediente : req.query["idExpediente"];
  let idSesion = (req.session.idSesion) ? req.session.idSesion : req.query["idSesion"];
  if (idExpediente && idSesion) {
    let code = 404;
    _endPoints.endPointsChunks.getIDValidas(idExpediente, idSesion)
      .then(exp => {
        code = exp.status;
        if (code < 205) {
          return exp.text();
        } else {
          try {
            return exp.text();
          } catch (error) {
            return exp.json();
          }
        }
      }).then(dato => {
        if (dato != undefined) {
          let reasigna = dato;
          console.log("retornando getidValidas " + reasigna);
          res.status(200).json({
            'idValidas': reasigna
          });
        }
      }).catch(er => {
        console.log("limpiando cookies");
        if (req.session) {
          req.session.destroy();
          res.clearCookie("monster");
        }
        res.status(503).json({
          err: er
        });
      });
  } else {
    res.status(401).json({
      err: "datosIncompletos"
    });
  }
});


app.post("/getIdValidas", parseForm, async function(req, res) {
  let idExpediente = (req.session.idExpediente) ? req.session.idExpediente : req.query["idExpediente"];
  let idSesion = (req.session.idSesion) ? req.session.idSesion : req.query["idSesion"];
  if (idExpediente && idSesion) {
    let code = 404;
    _endPoints.endPointsChunks.getIDValidas(idExpediente, idSesion)
      .then(exp => {
        code = exp.status;
        if (code < 205) {
          return exp.text();
        } else {
          try {
            return exp.text();
          } catch (error) {
            return exp.json();
          }
        }
      }).then(dato => {
        if (dato != undefined) {
          let reasigna = dato;
          console.log("retornando getidValidas " + reasigna + " " + dato);
          res.status(200).json({
            'idValidas': reasigna
          });
        }
      }).catch(er => {
        console.log("limpiando cookies");
        if (req.session) {
          req.session.destroy();
          res.clearCookie("monster");
        }
        res.status(503).json({
          err: er
        });
      });
  } else {
    res.status(401).json({
      err: "datosIncompletos"
    });
  }
});


app.post("/getId", async function(req, res) {
  let retorna = {};
  _endPoints.endPoints.getID().then(callBack => {
    retorna["status"] = callBack.status;
    if (callBack.status < 205) {
      return callBack.json();
    } else {
      return callBack.status();
    }
  }).then(dato => {
    if (typeof dato == "object") {
      res.status(retorna.status).json(dato);
    } else {
      res.status(retorna.status).send();
    }
  }).catch(er => {
    res.status(401).json({
      err: "getIDValidas " + er
    });
  });
});

app.post("/uploadAnv", upload.single('documentImage'), parseForm, async function(req, res) {
  if (req.query["idTransaccion"]) {
    if (req.file) {
      let retorna = {};
      _endPoints.endPoints.uploadAnv(req.query["idTransaccion"], req.file).then(callBack => {
        retorna["status"] = callBack.status;
        if (callBack.status < 205) {
          res.status(retorna.status).send();
        } else {
          return callBack.status();
        }
      }).then(dato => {
        if (typeof dato == "object") {
          res.status(retorna.status).json(dato);
        } else if (dato != undefined) {
          res.status(retorna.status).send();
        }
      }).catch(er => {
        res.status(500).json({
          err: "uploadAnv " + er
        });
      });
    } else {
      res.status(422).json({
        err: "File required"
      });
    }
  } else {
    res.status(422).json({
      err: "idTransaccion required"
    });
  }
});

app.post("/uploadRev", upload.single('documentImage'), parseForm, async function(req, res) {
  if (req.query["idTransaccion"]) {
    if (req.file) {
      let retorna = {};
      _endPoints.endPoints.uploadRev(req.query["idTransaccion"], req.file).then(callBack => {
        retorna["status"] = callBack.status;
        if (callBack.status < 205) {
          res.status(retorna.status).send();
        } else {
          return callBack.status();
        }
      }).then(dato => {
        if (typeof dato == "object") {
          res.status(retorna.status).json(dato);
        } else if (dato != undefined) {
          res.status(retorna.status).send();
        }
      }).catch(er => {
        res.status(500).json({
          err: "uploadRev " + er
        });
      });
    } else {
      res.status(422).json({
        err: "File required"
      });
    }
  } else {
    res.status(422).json({
      err: "idTransaccion required"
    });
  }
});

app.post("/improveImage", upload.single('documentImage'), parseForm, async function(req, res) {
  if (req.file) {
    let retorna = {};
    _endPoints.endPoints.uploadImproveImage(req.file).then(callBack => {
      retorna["status"] = callBack.status;
      // const dest = fs.createWriteStream('./octocat.png');
      // callBack.body.pipe(dest);
      if (callBack.status == 200) {
        return callBack.buffer();
        //res.status(retorna.status).send();
      } else {
        return callBack.status();
      }
    }).then(dato => {
      if (typeof dato == "object") {
        res.status(retorna.status).json(dato);
      } else if (dato != undefined) {
        res.status(retorna.status).send();
      }
    }).catch(er => {
      res.status(500).json({
        err: "uploadRev " + er
      });
    });
  } else {
    res.status(422).json({
      err: "File required"
    });
  }
});

app.post("/getOCR", async function(req, res) {
  if (req.query["idTransaccion"]) {
    let retorna = {};
    _endPoints.endPoints.getOCR(req.query["idTransaccion"]).then(callBack => {
      retorna["status"] = callBack.status;
      console.log(callBack.status);
      if (callBack.status < 205) {
        return callBack.json();
      } else {
        return callBack.status();
      }
    }).then(dato => {
      if (typeof dato == "object") {
        res.status(retorna.status).json(dato);
      } else {
        res.status(retorna.status).send();
      }
    }).catch(er => {
      res.status(401).json({
        err: "getOCR " + er
      });
    });
  } else {
    res.status(422).json({
      err: "idTransaccion required"
    });
  }
});

app.post("/getSCORES", async function(req, res) {
  if (req.query["idTransaccion"]) {
    let retorna = {};
    _endPoints.endPoints.getSCORES(req.query["idTransaccion"]).then(callBack => {
      retorna["status"] = callBack.status;
      console.log(callBack.status);
      if (callBack.status < 205) {
        return callBack.json();
      } else {
        return callBack.status();
      }
    }).then(dato => {
      if (typeof dato == "object") {
        res.status(retorna.status).json(dato);
      } else {
        res.status(retorna.status).send();
      }
    }).catch(er => {
      res.status(401).json({
        err: "getSCORES " + er
      });
    });
  } else {
    res.status(422).json({
      err: "idTransaccion required"
    });
  }
});


app.post("/dummyReverse", async function(req, res) {
  if (req.query["idTransaccion"]) {
    let retorna = {};
    var bitmap = fs.readFileSync("./reverse.jpeg");
    _endPoints.endPoints.uploadDummieRev(req.query["idTransaccion"], bitmap).then(callBack => {
      retorna["status"] = callBack.status;
      if (callBack.status < 205) {
        res.status(retorna.status).send();
      } else {
        return callBack.json();
      }
    }).then(dato => {
      if (typeof dato == "object") {
        res.status(retorna.status).json(dato);
      } else if (dato != undefined) {
        res.status(retorna.status).send();
      }
    }).catch(er => {
      res.status(500).json({
        err: "uploadRev " + er
      });
    });
  } else {
    res.status(422).json({
      err: "idTransaccion required"
    });
  }
});

let useHTTP = true;

if (!useHTTP) {
  const options = {
    key: fs.readFileSync("/Users/indra/Omar/CERTIFICADOS/server.key"),
    cert: fs.readFileSync("/Users/indra/Omar/CERTIFICADOS/server.crt")
  };

  _https.createServer(options, app)
    .listen(port, "0.0.0.0", () =>
      console.info("ONLINE"));
  const entorno = process.env.NODE_ENV || 'local';
  if (entorno != 'local') {
    _endPoints.endPoints.getConfig()
      .then(res => {
        return res.json();
      })
      .then(json => {
        global._globalConfig = json;
      })
      .catch(err => console.info(err));
  } else {
    global._globalConfig = _config.get('local');
  }
} else {
  app.listen(port, function() {
    const entorno = process.env.NODE_ENV || 'local';
    console.log('Example app listening on port ! ' + port + " " + entorno);
    if (entorno != 'local') {
      _endPoints.endPoints.getConfig()
        .then(res => {
          return res.json();
        })
        .then(json => {
          global._globalConfig = json;
        })
        .catch(err => console.info(err));
    } else {
      global._globalConfig = _config.get('local');
    }
  });
}
